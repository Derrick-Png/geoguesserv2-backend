import { supabase } from '../supabase/index.js';

export const signUp = async (email, username, password) => {
  // Check if username exists
  // const { data: sameUsername } = await supabase
  //   .from('users')
  //   .select('username')
  //   .eq('username', username);
  // if (sameUsername.length > 0) throw { error: 'Username taken' };

  // Create a new user in the auth schema (auth.users)
  const {
    data: { user: authUser, session },
    error: authUserError,
  } = await supabase.auth.signUp({
    email,
    password,
  });
  if (authUserError) {
    return { error: authUserError };
  }

  // Create a new user in the public schema (public.users)
  let { data: publicUser, error: publicUserError } = await supabase
    .from('user')
    .insert({ username, auth_id: authUser.id })
    .select();
  if (publicUserError) {
    return { error: publicUserError };
  }

  return { data: { user: publicUser[0], session } };
};
