import { supabase } from '../supabase/index.js';

export const signIn = async (email, password) => {
  const {
    data: { user: authUser, session },
    error: authUserError,
  } = await supabase.auth.signInWithPassword({
    email,
    password,
  });
  if (authUserError) {
    return { error: authUserError };
  }

  const { data, error: publicUserError } = await supabase
    .from('user')
    .select()
    .eq('auth_id', authUser.id);
  if (publicUserError) {
    return { error: publicUserError };
  }

  return { data: { user: data[0], session } };
};
