import express from 'express'
import cors from 'cors';
import { signUp } from './functions/sign-up.js'
import { signIn } from './functions/sign-in.js';

const app = express();
const port = 8080;

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.post('/signup', async (req, res) => {
  const { email, username, password } = req.body;
  const { data, error } = await signUp(email, username, password);
  
  if (data) {
    console.log(data);
    res.send({ statusCode: 200, data });
  } else {
    console.error(error);
    res.send({ statusCode: 400, error });
  }
});

app.post('/signin', async (req, res) => {
  const { email, password } = req.body;
  const { data, error } = await signIn(email, password);
  
  console.log(data);
  if (data) {
    console.log(data);
    res.send({ statusCode: 200, data });
  } else {
    console.error(error);
    res.send({ statusCode: 400, error });
  }
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
  console.log(`http://localhost:${port}`)
});